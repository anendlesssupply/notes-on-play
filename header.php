<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Notes_on_Play
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-barba="wrapper">
<?php locate_template("assets/svg/symbol-defs.svg", TRUE, TRUE); ?>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'notes-on-play' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle text-button" aria-controls="primary-menu" aria-expanded="false">
				<span class="screen-reader-text"><?php esc_html_e( 'Menu', 'notes-on-play' ); ?></span>
				<svg aria-hidden="true" class="icon icon-menu">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-menu"></use>
				</svg>
				<svg aria-hidden="true" class="icon icon-close">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-close"></use>
				</svg>
			</button>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'fallback_cb' => false,
					'container' => ''
				)
			);
			?>
		</nav><!-- #site-navigation -->
	
	</header><!-- #masthead -->

	<?php get_template_part( 'template-parts/site-announcements' ); ?> 
	
	<button class="front-menu-open text-button" aria-controls="primary-menu-front" aria-expanded="true">
		<span class="screen-reader-text"><?php esc_html_e( 'Menu', 'notes-on-play' ); ?></span>
		<svg aria-hidden="true" class="icon">
			<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-menu"></use>
		</svg>
	</button>
	<div class="site-header-front" aria-expanded="false">
		<button class="front-menu-close text-button" aria-controls="primary-menu-front" aria-expanded="false">
			<span class="screen-reader-text"><?php esc_html_e( 'Close', 'notes-on-play' ); ?></span>
			<svg aria-hidden="true" class="icon">
				<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-close"></use>
			</svg>
		</button>
		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu-front',
				'fallback_cb' => false,
				'container' => ''
			)
		);
		?>
		<footer>
			<div>
				<?php the_field('menu_intro_text', 'option'); ?>
			</div>
		</footer>
	</div>

	<div class="side-blocks side-blocks--left"></div>
	<div class="side-blocks side-blocks--right"></div>
	<span class="side-blocks-toggle"><span class="is-hidden">Show</span><span class="is-visible">Hide</span></span>

	<div class="site-container" data-barba="container" data-class="<?php echo join( ' ', get_body_class( ) ); ?>" data-barba-namespace="<?php echo get_the_title(); ?>">