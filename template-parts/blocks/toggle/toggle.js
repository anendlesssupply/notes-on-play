export function initToggleBlock( el ) {
	if ( ! el ) {
		return;
	}
	function handleClick( e ) {
		e.preventDefault();
		const content = el.querySelector( '.toggle-content' ) || null;
		if ( ! el.classList.contains( 'wp-block-acf-toggle--is-active' ) ) {
			el.classList.add( 'wp-block-acf-toggle--is-active' );
			button.setAttribute( 'aria-expanded', 'true' );
			content.setAttribute( 'aria-expanded', 'true' );
		} else {
			el.classList.remove( 'wp-block-acf-toggle--is-active' );
			button.setAttribute( 'aria-expanded', 'false' );
			content.setAttribute( 'aria-expanded', 'false' );
		}
	}
	const button = el.querySelector( '.toggle-button' ) || null;
	button.addEventListener( 'click', handleClick );
}

const initPreviewToggleBlock = ( el ) => {
	const block = el[ 0 ].querySelector( '.wp-block-acf-toggle' ) || null;
	if ( block ) {
		initToggleBlock( block );
	}
};

// Initialize dynamic block preview (editor).
if ( window.acf ) {
	window.acf.addAction(
		'render_block_preview/type=toggle',
		initPreviewToggleBlock
	);
}
