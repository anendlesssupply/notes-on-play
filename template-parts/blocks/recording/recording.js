export function initRecordingBlock( el ) {
	if ( ! el ) {
		return;
	}
	function handleClick( e ) {
		e.preventDefault();
		const text = el.querySelector( '.toggle-button__label' ) || null;
		const icon = el.querySelector( '.toggle-button__icon' ) || null;
		const content = el.querySelector( '.toggle-content' ) || null;
		if ( ! el.classList.contains( 'wp-block-acf-recording--is-active' ) ) {
			el.classList.add( 'wp-block-acf-recording--is-active' );
			button.setAttribute( 'aria-expanded', 'true' );
			content.setAttribute( 'aria-expanded', 'true' );
			if ( text ) {
				text.innerText = 'Read less';
			}
			if ( icon ) {
				icon.innerText = '-';
			}
		} else {
			el.classList.remove( 'wp-block-acf-recording--is-active' );
			button.setAttribute( 'aria-expanded', 'false' );
			content.setAttribute( 'aria-expanded', 'false' );
			if ( text ) {
				text.innerText = 'Read more';
			}
			if ( icon ) {
				icon.innerText = '+';
			}
		}
	}
	const button = el.querySelector( '.toggle-button' ) || null;
	button.addEventListener( 'click', handleClick );
}

const initPreviewRecordingBlock = ( el ) => {
	const block = el[ 0 ].querySelector( '.wp-block-acf-recording' ) || null;
	if ( block ) {
		initRecordingBlock( block );
	}
};

// Initialize dynamic block preview (editor).
if ( window.acf ) {
	window.acf.addAction(
		'render_block_preview/type=recording',
		initPreviewRecordingBlock
	);
}
