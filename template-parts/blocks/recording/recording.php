<?php
$id = 'wp-block-acf-recording-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}
$className = 'wp-block-acf-recording';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$title = get_field('title') ? get_field('title') : '';
$audio = get_field('audio') ? get_field('audio') : '';
$read_more_content = get_field('read_more_content') ? get_field('read_more_content') : '';

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <?php if($title): ?>
        <h2><?php echo $title; ?></h2>
    <?php endif; ?>
    <?php if($audio): ?>
        <div class="wp-block-acf-recording__audio">
            <audio
                controls
                controlsList="nodownload"
                src="<?php echo $audio['url']; ?>">
                Your browser doesn't support HTML5 audio.
                Here is a <a href="<?php echo $audio['url']; ?>">link to download the audio</a> instead.
            </audio>
        </div>
    <?php endif; ?>
    <?php if($read_more_content): ?>
        <div id="<?php echo 'toggle-content-' . $id; ?>" class="toggle-content" aria-expanded="false">
            <?php echo $read_more_content; ?>
        </div>
        <button class="toggle-button" aria-controls="<?php echo 'toggle-content-' . $id; ?>" aria-expanded="false">
            <span class="toggle-button__label">Read more</span>
            <span class="toggle-button__icon" aria-hidden="true">
                +
            </span>
        </button>
    <?php endif; ?>
</div>