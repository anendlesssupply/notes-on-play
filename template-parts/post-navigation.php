
<?php
$current_id = get_the_ID();
$post_type = get_post_type();
$all_posts = new WP_Query( 
    array( 
        'post_type' => $post_type,
        'posts_per_page' => '-1',
        'fields' => 'ids'
    )
);
$all_ids = [];
if ( $all_posts->have_posts() ):
    while ( $all_posts->have_posts() ):
    $all_posts->the_post();
        $all_ids[] =  $post;
    endwhile;
endif;
$current_index = array_search ( $current_id , $all_ids , false );
$total_posts = count($all_ids);
if($current_index - 1 < 0){
    $prev_id = $all_ids[$total_posts - 1];
} else {
    $prev_id = $all_ids[$current_index - 1];
}
if($current_index >= $total_posts - 1){
    $next_id = $all_ids[0];
} else {
    $next_id = $all_ids[$current_index + 1];
}
if($prev_id || $prev_id): ?>
    <nav class="navigation post-navigation" role="navigation" aria-label="Posts">
        <h2 class="screen-reader-text">Post navigation</h2>
        <div class="nav-links">
            <?php if($prev_id): ?>
                <div class="nav-previous">
                    <a href="<?php echo get_the_permalink($prev_id); ?>" rel="prev">
                        <span class="nav-subtitle">Previous:</span> 
                        <span class="nav-title">
                            <?php
                            $post_title = get_the_title($prev_id);
                            echo $post_title; ?>
                        </span>
                    </a>
                </div>
            <?php endif; ?>
            <?php if($next_id): ?>
                <div class="nav-next">
                    <a href="<?php echo get_the_permalink($next_id); ?>" rel="next">
                        <span class="nav-subtitle">Next:</span> 
                        <span class="nav-title">
                            <?php
                            $post_title = get_the_title($next_id);
                            echo $post_title; ?>
                        </span>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </nav>
<?php endif;
wp_reset_postdata();