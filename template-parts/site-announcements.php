<?php 
$today = date("Ymd");
$args = array(
  'post_type' => 'announcement',
  'posts_per_page' => -1,
  'orderby'   => 'menu_order',
  'order' => 'DESC',
);
$announcements = new WP_Query( $args );
$announcements_data = array();
if( $announcements ):
	while( $announcements->have_posts() ): $announcements->the_post();
    if( get_field('date_end') ):
      if(strtotime( get_field('date_end') ) > strtotime( date('Ymd') ) ):
        $announcements_data[] = array(
          "announcement_id"		=> $post->ID,
          "announcement_content"	=> get_field('message'),
          "announcement_updated"	=> get_the_modified_date('YmdGis'),
        );
      endif;
    else:
      $announcements_data[] = array(
        "announcement_id"		=> $post->ID,
        "announcement_content"	=> get_field('message'),
        "announcement_updated"	=> get_the_modified_date('YmdGis'),
      );
		endif;
  endwhile;
  wp_reset_postdata();
endif;

if($announcements_data): ?>
  <div class="site-announcements">
    <?php foreach($announcements_data as $announcement): ?>
      <div class="site-announcement" data-id="<?php echo $announcement['announcement_id']; ?>" data-updated="<?php echo $announcement['announcement_updated']; ?>">
        <button class="close-announcement text-button" type="button">
          <span class="screen-reader-text">Clear announcement</span>
          <svg class="icon">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-close"></use>
          </svg>
        </button>
        <div class="announcement-inner">
          <?php echo $announcement['announcement_content']; ?>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <?php wp_reset_postdata();
endif;
