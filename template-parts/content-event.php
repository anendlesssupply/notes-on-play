<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Notes_on_Play
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
        $date = get_field('date') ?: '';
        $booking_link = get_field('booking_link') ?: '';
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
            if($date):
                echo '<h2 class="entry-title">' . $date . '</h2>';
            endif;
            if( $booking_link ): 
                $link_url = $booking_link['url'];
                $link_title = $booking_link['title'];
                $link_target = $booking_link['target'] ? $booking_link['target'] : '_self';
                ?>
                <a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
            <?php endif;
		else : ?>
            <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" class="entry-title staggered-text staggered-text--align-center"><?php the_title( '<h2>', '</h2>' ); if($date): echo '<h3 class="staggered-text--ignore">' . $date . '</h3>'; endif; ?></a>
        <?php endif; ?>
	</header><!-- .entry-header -->

	<?php // notes_on_play_post_thumbnail(); ?>
    <?php if ( is_singular() ) : ?>
        <div class="entry-content">
            <?php
            the_content(
                sprintf(
                    wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers */
                        __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'notes-on-play' ),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    wp_kses_post( get_the_title() )
                )
            );
            ?>
        </div><!-- .entry-content -->

        <footer class="entry-footer">
            <?php notes_on_play_entry_footer(); ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
