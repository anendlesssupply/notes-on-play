<?php
add_action( 'init', 'my_announcement_cpt' );
function my_announcement_cpt() {
  $labels = array(
    'name'               => _x( 'Announcements', 'post type general name', 'notes-on-play' ),
    'singular_name'      => _x( 'Announcement', 'post type singular name', 'notes-on-play' ),
    'menu_name'          => _x( 'Announcements', 'admin menu', 'notes-on-play' ),
    'name_admin_bar'     => _x( 'Announcements', 'add new on admin bar', 'notes-on-play' ),
    'add_new'            => _x( 'Add New', 'Announcement', 'notes-on-play' ),
    'add_new_item'       => __( 'Add New Announcement', 'notes-on-play' ),
    'new_item'           => __( 'New Announcement', 'notes-on-play' ),
    'edit_item'          => __( 'Edit Announcement', 'notes-on-play' ),
    'view_item'          => __( 'View Announcement', 'notes-on-play' ),
    'all_items'          => __( 'All Announcements', 'notes-on-play' ),
    'search_items'       => __( 'Search Announcements', 'notes-on-play' ),
    'parent_item_colon'  => __( 'Parent Announcement:', 'notes-on-play' ),
    'not_found'          => __( 'No Announcements found.', 'notes-on-play' ),
    'not_found_in_trash' => __( 'No Announcements found in Trash.', 'notes-on-play' )
  );
 
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Announcements', 'notes-on-play' ),
    'public'             => true,
    'publicly_queryable' => false,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'with_front' => false, 'slug' => 'announcements' ),
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => 5,
    'menu_icon'          => 'dashicons-art',
    'show_in_rest'       => true,
    'supports'           => array( 'title' )
  );
 
  register_post_type( 'announcement', $args );
}


add_filter( 'manage_edit-announcement_columns', 'my_edit_announcement_columns' ) ;

function my_edit_announcement_columns( $columns ) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title', 'notes-on-play' ),
		'end_date' => __( 'Date End', 'notes-on-play' ),
		'date' => __( 'Posted', 'notes-on-play' )
	);
	return $columns;
}
add_action( 'manage_announcement_posts_custom_column', 'my_manage_announcement_columns', 10, 2 );

function my_manage_announcement_columns( $column, $post_id ) {
	global $post;
	$today = date('Ymd');
	switch( $column ) {
		case 'end_date' :
			$end_date = get_post_meta( $post_id, 'date_end', true ) ?: "";
      if ( empty( $end_date ) ){
        echo __( '<span style="color:#ffbf00">No end date</span>', 'notes-on-play' );
      } else {					
        $value = date("Y/m/d", strtotime($end_date));
        if($end_date >= $today){
          printf( __( '<span style="color:#00dd00">%s</span>', 'notes-on-play' ), $value );
        } else {
          printf( __( '%s', 'notes-on-play' ), $value );
        }
      }
			break;
		default :
			break;
	}
}

