<?php
add_action( 'init', 'my_event_cpt' );
function my_event_cpt() {
  $labels = array(
    'name'               => _x( 'Events', 'post type general name', 'notes-on-play' ),
    'singular_name'      => _x( 'Event', 'post type singular name', 'notes-on-play' ),
    'menu_name'          => _x( 'Events', 'admin menu', 'notes-on-play' ),
    'name_admin_bar'     => _x( 'Events', 'add new on admin bar', 'notes-on-play' ),
    'add_new'            => _x( 'Add New', 'Event', 'notes-on-play' ),
    'add_new_item'       => __( 'Add New Event', 'notes-on-play' ),
    'new_item'           => __( 'New Event', 'notes-on-play' ),
    'edit_item'          => __( 'Edit Event', 'notes-on-play' ),
    'view_item'          => __( 'View Event', 'notes-on-play' ),
    'all_items'          => __( 'All Events', 'notes-on-play' ),
    'search_items'       => __( 'Search Events', 'notes-on-play' ),
    'parent_item_colon'  => __( 'Parent Event:', 'notes-on-play' ),
    'not_found'          => __( 'No Events found.', 'notes-on-play' ),
    'not_found_in_trash' => __( 'No Events found in Trash.', 'notes-on-play' )
  );
 
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Events', 'notes-on-play' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'with_front' => false, 'slug' => 'events' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 5,
    'menu_icon'          => 'dashicons-art',
    'show_in_rest'       => true,
    'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail' )
  );
 
  register_post_type( 'event', $args );
}
